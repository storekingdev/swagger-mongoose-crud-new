'use strict';

var _ = require('lodash');
var BaseController = require('./base.controller');
var params = require('./swagger.params.map');
const moment = require('moment');
const mongoose = require('mongoose');
const jwtDecode = require('jwt-decode');
const cuti = require("cuti");
const statusCodes = cuti.constants.statusCodes;
const errMsg = require("./utils").errorMessage;
const utils = require("./utils");
const mapping = require("./utils").collectionMapping;
/**
 * Constructor function for CrudController.
 * @classdesc Controller for basic CRUD operations on mongoose models.
 * Uses the passed id name as the request parameter id to identify models.
 * @constructor
 * @inherits BaseController
 * @param {Model} model - The mongoose model to operate on
 * @param {String} [idName] - The name of the id request parameter to use
 */
function CrudController(model, logger) {
    // call super constructor
    BaseController.call(this, this);

    // set the model instance to work on
    this.model = model;
    this.logger = logger;
    // set id name if defined, defaults to 'id'
    this.omit = [];
    _.bindAll(this);
}

CrudController.prototype = {

    /**
     * Set our own constructor property for instanceof checks
     * @private
     */
    constructor: CrudController,

    /**
     * The model instance to perform operations with
     * @type {MongooseModel}
     */
    model: null,

    /**
     * The id  parameter name
     * @type {String}
     * @default 'id'
     */
    idName: 'id',


    /**
     * Flag indicating whether the index query should be performed lean
     * @type {Boolean}
     * @default true
     */
    lean: true,

    /**
     * Array of fields passed to the select statement of the index query.
     * The array is joined with a whitespace before passed to the select
     * method of the controller model.
     * @type {Array}
     * @default The empty Array
     */
    select: [],

    /**
     * Array of fields that should be omitted from the query.
     * The property names are stripped from the query object.
     * @type {Array}
     * @default The empty Array
     */
    omit: [],

    /**
     * Name of the property (maybe a virtual) that should be returned
     * (send as response) by the methods.
     * @type {String}
     * @default The empty String
     */
    defaultReturn: '',
    auditLogger: function (doc, body) {
        var intersection = _.pick(doc, _.keysIn(body));
        this.logger.audit('Object with id :-' + doc._id + ' has been updated, old values:-' + JSON.stringify(intersection) + ' new values:- ' + JSON.stringify(body));
    },
    /**
     * Default Data handlers for Okay Response
     * @type {function}
     * @default Okay response.
     */
    Okay: function (req, res, data, collectionName, hasNext, next) {
        if (hasNext === true) {
            next(data);
        } else {
            res.status(200).json(data);
        }


    },

    writeLog: function (_reqId, _logType, _operation, _user, _payload, _timestamp, _host, _resposne) {
        this.logger.info(JSON.stringify({
            reqId: _reqId,
            logType: _logType,
            operation: _operation,
            user: _user,
            payload: _payload,
            host: _host,
            timestamp: _timestamp,
            response: _resposne
        }));
    },

    writeError: function (_reqId, _operation, _err) {
        try {
            if (_err.message && typeof _err.message === "string" && JSON.parse(_err.message).statusCode && JSON.parse(_err.message).statusCode === 103) {
                _err = _err.message;
            }
        } catch (err) { }
        this.logger.error(JSON.stringify({
            reqId: _reqId,
            operation: _operation,
            error: _err
        }));
    },
    /**
     * Default Data handlers for Okay Response
     * @type {function}
     * @default Okay response.
     */
    NotFound: function (id, res, collectionName, hasNext, next) {
        // let msg = errMsg.recordNotFound.msg;
        let msg = utils.recordNotFound(id);
        let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.recordNotFound.code;
        let resObj = {
            message: msg,
            statusCode: code,
            status: 'NOT-FOUND'
        };
        res.status(404).json(resObj);
    },

    IsString: function (val) {
        return val && val.constructor.name === 'String';
    },
    CreateRegexp: function (str) {
        if (str.charAt(0) === '/' &&
            str.charAt(str.length - 1) === '/') {
            var text = str.substr(1, str.length - 2).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            return new RegExp(text, 'i');
        } else {
            return str;
        }
    },
    IsArray: function (arg) {
        return arg && arg.constructor.name === 'Array';
    },
    IsObject: function (arg) {
        return arg && arg.constructor.name === 'Object';
    },
    ResolveArray: function (arr) {
        var self = this;
        for (var x = 0; x < arr.length; x++) {
            if (self.IsObject(arr[x])) {
                arr[x] = self.FilterParse(arr[x]);
            } else if (self.IsArray(arr[x])) {
                arr[x] = self.ResolveArray(arr[x]);
            } else if (self.IsString(arr[x])) {
                arr[x] = self.CreateRegexp(arr[x]);
            }
        }
        return arr;
    },
    /*
     * Takes the filter field and parses it to a JSON object
     * @type {function}
     *  
     */
    FilterParse: function (filterParsed) {
        var self = this;
        for (var key in filterParsed) {
            if (self.IsString(filterParsed[key])) {
                filterParsed[key] = self.CreateRegexp(filterParsed[key]);
            } else if (self.IsArray(filterParsed[key])) {
                filterParsed[key] = self.ResolveArray(filterParsed[key]);
            } else if (self.IsObject(filterParsed[key])) {
                filterParsed[key] = self.FilterParse(filterParsed[key]);
            }
        }
        return filterParsed;
    },
    /**
     * Default Data handlers for Okay Response
     * @type {function}
     * @default Okay response.
     */
    Error: function (res, err, collectionName) {
        // console.log("==>>> " ,  err)
        if (err.errors && Object.keys(err.errors).length && Object.keys(err.errors).length != 0) {
            var errors = [];
            Object.keys(err.errors).forEach(el => errors.push(err.errors[el].message));
            let msg = errMsg.commonError.msg;
            let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.errorOcurred.code;
            res.status(400).json({
                message: msg,
                error: errors,
                status: err.status,
                statusCode: code
            });
        } else if (err.statusCode && err.statusCode === 103) {
            let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.duplicate.code;
            res.status(400).json({
                message: err,
                status: statusCodes.DUPLICATE.status,
                statusCode: code
            });
        } else {
            if (err.message && err.code) {
                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.commonError.code;
                res.status(400).json({
                    message: err.message,
                    status: statusCodes.Something_went_wrong.status,
                    statusCode: code
                });
            } else if (err.message && typeof err.message === "string") {
                let newErr = _.cloneDeep(err.message.replace(/["']/g, ""));
                let value = (newErr.split(" ", 2).join(" "));
                if (value === "Cast to") {
                    let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.commonError.code;
                    res.status(400).json({
                        message: newErr,
                        status: statusCodes.Something_went_wrong.status,
                        statusCode: code
                    });
                } else {
                    if (err.message && typeof err.message === "string" && JSON.parse(err.message).statusCode && JSON.parse(err.message).statusCode === 103) {
                        let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.duplicate.code;
                        res.status(406).json({
                            message: JSON.parse(err.message),
                            status: statusCodes.DUPLICATE.status,
                            statusCode: code
                        });
                    } else {
                        let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.commonError.code;
                        res.status(400).json({
                            message: [err.message],
                            status: statusCodes.Something_went_wrong.status,
                            statusCode: code
                        });
                    }
                }
            } else {
                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.commonError.code;
                res.status(400).json({
                    message: [err.message],
                    status: statusCodes.Something_went_wrong.status,
                    statusCode: code
                });
            }
        }
    },
    /**
     * Get a count of results matching a particular filter criteria.
     * @param {IncomingMessage} req - The request message object
     * @param {ServerResponse} res - The outgoing response object the result is set to
     */
    _count: function (req, res, hasNext, next) {
        var self = this;
        var reqParams = params.map(req);
        var filter = reqParams['filter'] ? reqParams.filter : {};
        if (typeof filter === 'string') {
            try {
                filter = JSON.parse(filter);
                filter = self.FilterParse(filter);
            } catch (err) {
                this.logger.error('Failed to parse filter :' + err);
                filter = {};
            }
        }
        if (this.omit.length > 0) {
            filter = _.omit(filter, this.omit);
        }
        filter.deleted = false;
        let collectionName = this.model.collection.collectionName;
        this.model.find(filter).count().exec().then(result => self.Okay(req, res, result, hasNext, next),
            err => self.Error(res, err, collectionName));
    },
    /**
     * Get a list of documents. If a request query is passed it is used as the
     * query object for the find method.
     * @param {IncomingMessage} req - The request message object
     * @param {ServerResponse} res - The outgoing response object the result is set to
     * @returns {ServerResponse} Array of all documents for the {@link CrudController#model} model
     * or the empty Array if no documents have been found
     */
    _index: function (req, res, hasNext, next) {
        var reqId = Math.random().toString(36).slice(8).toUpperCase();
        var reqParams = params.map(req);
        var filter = reqParams.filter ? reqParams.filter : {};
        var collectionName = this.model.collection.collectionName;
        var sort = reqParams.sort ? {} : {
            lastUpdated: -1
        };
        this.writeLog(reqId, "Request", "GET", req.user ? req.user.username : req.headers['masterName'], JSON.parse(JSON.stringify(filter)), new Date(), req.ip, "");
        reqParams.sort ? reqParams.sort.split(',').map(el => el.split('-').length > 1 ? sort[el.split('-')[1]] = -1 : sort[el.split('-')[0]] = 1) : null;
        var select = reqParams.select ? reqParams.select.split(',') : [];

        var self = this;
        if (typeof filter === 'string') {
            try {
                filter = JSON.parse(filter);

                if (filter.status && typeof filter.status === 'string') {
                    try {
                        filter.status = JSON.parse(filter.status);
                    } catch (err) {
                        filter.status = filter.status;
                    }
                }
                filter = self.FilterParse(filter);

            } catch (err) {
                this.logger.error('Failed to parse filter :' + err);
                filter = {};
            }
        }

        if (this.omit.length) {
            filter = _.omit(filter, this.omit);
        }
        filter.deleted = false;
        

        this.logger.info("Filter to be used for search : ", JSON.stringify(filter))
        var query = this.model.find(filter);

        if (this.lean) {
            query.lean();
        }

        if (this.select.length || select.length) {
            var union = this.select.concat(select);
            query.select(union.join(' '));
        }

        // Pagination
        if (reqParams.page && reqParams.count) {
            var page = reqParams.page;
            var count = reqParams.count;
            var skip = count * (page - 1);
            query.skip(skip).limit(count);
        }

        if (reqParams.distinct && !select.length) {
            query.distinct(reqParams.distinct)
        } else {
            query.sort(sort)
        }
        //query.collation({locale: "en" }).sort(sort).exec((err, documents) => {
        // query.sort(sort).exec((err, documents) => {
        query.exec((err, documents) => {
            if (err) {
                this.writeError(reqId, 'Index', err);
                return self.Error(res, err, collectionName);
            }
            if (reqParams.page && reqParams.count) {
                this.model.count(filter).exec((err, totalCount) => {
                    if (err) {
                        this.writeError(reqId, 'Index', err);
                        return self.Error(res, err, collectionName);
                    } else {

                        //Authorizing Document
                        if (req.authorizeFlag && req.permission) {
                            const permission = req.permission;
                            if (permission.granted) {
                                documents = JSON.parse(JSON.stringify(documents))

                                //Convert distinct response into json array with name
                                if (req.swagger.params['distinct'] && req.swagger.params['distinct'].value) {
                                    var distinctVal = req.swagger.params['distinct'].value;
                                    var obj = {}
                                    obj[distinctVal] = documents
                                    documents = [obj];
                                }
                                if (!Array.isArray(documents)) {
                                    var payloadArray = [];
                                    payloadArray.push(documents)
                                    documents = payloadArray
                                }
                                //Filter document
                                var filteredPayload = documents.map(cuti.request.authorizationfilter(permission.attributes))

                                if (filteredPayload[0] && Object.keys(filteredPayload[0]).length != 0)
                                    documents = filteredPayload;
                                else {
                                    self.writeError(reqId, 'Read', 'Not authorized: ' + reqParams['id']);
                                    let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                                    res.status(401).send({
                                        'status': statusCodes.UNAUTHORIZED.status,
                                        'statusCode': code,
                                        'message': "You don't have access privileges.."
                                    })
                                    return;
                                }
                            } else {
                                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                                res.status(401).send({
                                    'status': statusCodes.UNAUTHORIZED.status,
                                    'statusCode': code,
                                    'message': "You don't have access privileges.."
                                })
                                return;
                            }
                        }

                        return self.Okay(req, res, {
                            totalCount: totalCount,
                            data: documents
                        }, collectionName, hasNext, next);
                    }
                });
            } else {

                //Authorizing Document
                if (req.authorizeFlag && req.permission) {
                    const permission = req.permission;
                    if (permission.granted) {
                        documents = JSON.parse(JSON.stringify(documents))

                        //Convert distinct response into json array with name
                        if (req.swagger.params['distinct'] && req.swagger.params['distinct'].value) {
                            var distinctVal = req.swagger.params['distinct'].value;
                            var obj = {}
                            obj[distinctVal] = documents
                            documents = [obj];
                        }
                        if (!Array.isArray(documents)) {
                            var payloadArray = [];
                            payloadArray.push(documents)
                            documents = payloadArray
                        }
                        //Filter document
                        var filteredPayload = documents.map(cuti.request.authorizationfilter(permission.attributes))

                        if (filteredPayload[0] && Object.keys(filteredPayload[0]).length != 0)
                            documents = filteredPayload;
                        else {
                            self.writeError(reqId, 'Read', 'Not authorized ');
                            let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                            res.status(401).send({
                                'status': statusCodes.UNAUTHORIZED.status,
                                'statusCode': code,
                                'message': "You don't have access privileges.."
                            })
                            return;
                        }
                    } else {
                        self.writeError(reqId, 'Read', 'Not authorized ');
                        let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                        res.status(401).send({
                            'status': statusCodes.UNAUTHORIZED.status,
                            'statusCode': code,
                            'message': "You don't have access privileges.."
                        })
                        return;
                    }
                }
                this.writeLog(reqId, "Response", "Index", req.user ? req.user.username : req.headers['masterName'], filter, new Date(), req.ip, {
                    data: documents
                });
                return self.Okay(req, res, {
                    data: documents
                }, collectionName, hasNext, next);
            }
        })
    },

    /**
     * Get a single document. The requested document id is read from the request parameters
     * by using the {@link CrudController#idName} property.
     * @param {IncomingMessage} req - The request message object the id is read from
     * @param {ServerResponse} res - The outgoing response object
     * @returns {ServerResponse} A single document or NOT FOUND if no document has been found
     */
    _show: function (req, res, hasNext, next) {
        var reqId = Math.random().toString(36).slice(8).toUpperCase();
        var self = this;
        this.writeLog(reqId, "Request", "GET", req.user ? req.user.username : req.headers['masterName'], params.map(req)['id'], new Date(), req.ip, "");
        var reqParams = params.map(req);
        var select = reqParams['select'] ? reqParams.select.split(',') : []; //Comma seprated fileds list
        let collectionName = this.model.collection.collectionName;
        var query = this.model.findOne({
            '_id': reqParams['id'],
            deleted: false
        });
        if (select.length > 0) {
            query = query.select(select.join(' '));
        }
        query.exec().then((document) => {
            if (!document) {
                this.writeError(reqId, 'Show', 'No Document Found with id: ' + reqParams['id']);
                return self.NotFound(reqParams['id'], res, collectionName, hasNext, next);
            } else {

                //Authorizing Document
                if (req.authorizeFlag && req.permission) {
                    const permission = req.permission;
                    if (permission.granted) {
                        document = JSON.parse(JSON.stringify(document))

                        //Convert distinct response into json array with name
                        if (req.swagger.params['distinct'] && req.swagger.params['distinct'].value) {
                            var distinctVal = req.swagger.params['distinct'].value;
                            var obj = {}
                            obj[distinctVal] = document
                            document = [obj];
                        }
                        if (!Array.isArray(document)) {
                            var payloadArray = [];
                            payloadArray.push(document)
                            document = payloadArray
                        }
                        //Filter document
                        var filteredPayload = document.map(cuti.request.authorizationfilter(permission.attributes))

                        if (filteredPayload[0] && Object.keys(filteredPayload[0]).length != 0)
                            document = filteredPayload;
                        else {
                            self.writeError(reqId, 'Read', 'Not authorized: ' + reqParams['id']);
                            let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                            res.status(401).send({
                                'status': statusCodes.UNAUTHORIZED.status,
                                'statusCode': code,
                                'message': "You don't have access privileges.."
                            })
                            return;
                        }
                    } else {
                        self.writeError(reqId, 'Read', 'Not authorized: ' + reqParams['id']);
                        let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                        res.status(401).send({
                            'status': statusCodes.UNAUTHORIZED.status,
                            'statusCode': code,
                            'message': "You don't have access privileges.."
                        })
                        return;
                    }
                }
                this.writeLog(reqId, "Response", "Show", req.user ? req.user.username : req.headers['masterName'], reqParams['id'], new Date(), req.ip, document);
                return self.Okay(req, res, self.getResponseObject(document), collectionName, hasNext, next);
            }
        }, err => {
            this.writeError(reqId, 'Show', err);
            self.Error(res, err, collectionName);
        });
    },

    /**
     * Creates a new document in the DB.
     * @param {IncomingMessage} req - The request message object containing the json document data
     * @param {ServerResponse} res - The outgoing response object
     * @returns {ServerResponse} The response status 201 CREATED or an error response
     */
    _create: function (req, res, hasNext, next) {
        var reqId = Math.random().toString(36).slice(8).toUpperCase();
        var self = this;
        self.writeLog(reqId, "Request", "POST", req.user ? req.user.username : req.headers['masterName'], params.map(req)['data'], new Date(), req.ip, "");
        var payload = 'data';
        var body = params.map(req)[payload];
        var collectionName = this.model.collection.collectionName;
        //Authorize and filter data
        if (req.authorizeFlag && req.permission) {
            const permission = req.permission;
            if (permission.granted) {
                //Filter payload request based on permission attributes
                var payloadArray = [];
                payloadArray.push(body)
                body = payloadArray.map(cuti.request.authorizationfilter(permission.attributes))
                body = body[0]

            } else {
                self.writeError(reqId, 'Create', 'Not authorized while creating');
                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                
                res.status(401).send({
                    'status': statusCodes.UNAUTHORIZED.status,
                    'statusCode': code,
                    'message': "You don't have access privileges.."
                })
                return;
            }
        }

        if (!body.createdBy) body.createdBy = (req.session && req.session.user.user._id) ? req.session.user.user._id : "";
        // body.createdBy = (req.session && req.session.user.user._id) ? req.session.user.user._id : "";
        body.versionNumber = 0;
        if (!body.createdAt) body.createdAt = moment().utc().local().format('YYYY-MM-DD HH:mm:ss');
        body.lastUpdated = moment().utc().local().format('YYYY-MM-DD HH:mm:ss');

        this.model.create(body, function (err, document) {
            if (err) {
                self.writeError(reqId, 'Create', err);
                return self.Error(res, err, collectionName);
            }
            self.writeLog(reqId, 'Response', 'Create', req.user ? req.user.username : req.headers['masterName'], body, new Date(), req.ip, document);
            return self.Okay(req, res, self.getResponseObject(document), collectionName, hasNext, next);
        });
    },


    _bulkShow: function (req, res, hasNext, next) {
        var sort = {};
        var reqParams = params.map(req);
        var ids = reqParams['id'].split(',');
        reqParams['sort'] ? reqParams.sort.split(',').map(el => sort[el] = 1) : null;
        var select = reqParams['select'] ? reqParams.select.split(',') : null;
        var query = {
            '_id': {
                '$in': ids
            },
            'deleted': false
        };
        var self = this;
        var mq = this.model.find(query);
        let collectionName = this.model.collection.collectionName;
        if (select) {
            mq = mq.select(select.join(' '));
        }
        return mq.sort(sort).exec().then(result => self.Okay(req, res, result, hasNext, next), err => this.Error(res, err, collectionName));
    },

    _updateMapper: function (id, body, user) {
        var self = this;
        return new Promise((resolve, reject) => {
            self.model.findOne({
                '_id': id,
                deleted: false
            }, function (err, doc) {
                if (err) {
                    reject(err);
                } else if (!doc) {
                    reject(new Error('Document not found'));
                } else {
                    var oldValues = doc.toObject();
                    var updated = _.mergeWith(doc, body, self._customizer);
                    updated = new self.model(updated);
                    Object.keys(body).forEach(el => updated.markModified(el));
                    updated.save(function (err) {
                        if (err) {
                            reject(err);
                        }
                        var logObject = {
                            'operation': 'Update',
                            'user': user,
                            'originalValues': oldValues,
                            '_id': doc._id,
                            'newValues': body,
                            'timestamp': new Date()
                        };
                        self.logger.audit(JSON.stringify(logObject));
                        resolve(updated);
                    });
                }
            }).exec();
        });
    },

    _bulkUpdate: function (req, res) {
        var reqParams = params.map(req);
        var body = reqParams['data']; //Actual transformation
        var selectFields = Object.keys(body);
        var self = this;
        selectFields.push('_id');
        var ids = reqParams['id'].split(','); //Ids will be comma seperated ID list
        var user = req.user ? req.user.username : req.headers['masterName'];
        var promises = ids.map(id => self._updateMapper(id, body, user));
        var promise = Promise.all(promises).then(result => res.json(result), err => {
            self.Error(res, err, "");
        });
        return promise;
    },

    _bulkUpload: function (req, res) {
        try {
            let buffer = req.files.file[0].buffer.toString('utf8');
            let rows = buffer.split('\n');
            let keys = rows[0].split(',');
            let products = [];
            let self = this;
            rows.splice(0, 1);
            rows.forEach(el => {
                let values = el.split(',');
                values.length > 1 ? products.push(_.zipObject(keys, values)) : null;
            });
            Promise.all(products.map(el => self._bulkPersist(el))).
                then(result => res.status(200).json(result));
        } catch (e) {
            res.status(400).json(e);
        }
    },

    _bulkPersist: function (el) {
        var self = this;
        return new Promise((res, rej) => {
            self.model.create(el, function (err, doc) {
                if (err)
                    res(err);
                else
                    res(doc);
            });
        });
    },

    /**
     * Updates an existing document in the DB. The requested document id is read from the
     * request parameters by using the {@link CrudController#idName} property.
     * @param {IncomingMessage} req - The request message object the id is read from
     * @param {ServerResponse} res - The outgoing response object
     * @params {String} in -  The Body payload location, if not specified, the parameter is assumed to be 'body'
     * @returns {ServerResponse} The updated document or NOT FOUND if no document has been found
     */
    _update: function (req, res, hasNext, next) {
        var reqId = Math.random().toString(36).slice(8).toUpperCase();
        var self = this;
        var reqParams = params.map(req);
        var bodyIn = 'data';
        var body = reqParams[bodyIn];
        var bodyData = _.omit(body, this.omit);
        
        let collectionName = this.model.collection.collectionName;
        //Authorize and filter data
        if (req.authorizeFlag && req.permission) {
            const permission = req.permission;
            if (permission.granted) {
                //Filter payload request based on permission attributes
                var payloadArray = [];
                payloadArray.push(bodyData)
                bodyData = payloadArray.map(cuti.request.authorizationfilter(permission.attributes))
                bodyData = bodyData[0]
            } else {
                self.writeError(reqId, 'Update', 'Not authorized: ' + reqParams['id']);
                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                res.status(401).send({
                    'status': statusCodes.UNAUTHORIZED.status,
                    'statusCode': code,
                    'message': "You don't have access privileges.."
                })
                return;
            }
        }

        self.writeLog(reqId, "Request", "PUT", req.user ? req.user.username : req.headers['masterName'], reqParams['data'], new Date(), req.ip, "");
        
        this.model.findOne({
            '_id': reqParams['id'],
            deleted: false
        }, function (err, document) {
            if (err) {
                self.writeError(reqId, 'Update', err);
                return self.Error(res, err, collectionName);
            }

            if (!document) {
                self.writeError(reqId, 'Update', 'No Document Found with id: ' + reqParams['id']);
                return self.NotFound(reqParams['id'], res, collectionName, hasNext, next);
            }
            var oldValues = document.toObject();
            document = document.toObject();

            bodyData.createdAt = document.createdAt;
            bodyData.createdBy = document.createdBy;

            // version fields 
            bodyData.versionNumber = Number(document.versionNumber ? document.versionNumber + 1 : 1);
            let tmpObj = {
                versionNumber: bodyData.versionNumber,
                versionRemark: bodyData.versionRemark || "",
                createdAt: moment().utc().local().format('YYYY-MM-DD HH:mm:ss')
            };
            let updatedBy;
            if (req.session && req.session.user && req.session.user.user && req.session.user.user._id) {
                updatedBy = req.session.user.user._id;
            } else if (req.session && req.session.user && req.session.user._id) {
                updatedBy = req.session.user._id;
            } else {
                if (req.headers.authorization) {
                    var token = req.headers.authorization;
                    token = token.split(" ")[1];
                    let user = jwtDecode(token);
                    if (user && user.user && user.user._id) {
                        updatedBy = user.user._id;
                    }
                }
            }
            tmpObj["updatedBy"] = updatedBy;
            bodyData.versionHistory = document.versionHistory ? document.versionHistory : [];
            bodyData.versionHistory.push(tmpObj);;

            bodyData.lastUpdated = moment().utc().local().format('YYYY-MM-DD HH:mm:ss');
            // bodyData.lastUpdatedBy = (req.session && req.session.user.user._id) ? req.session.user.user._id : "";
            // bodyData.__v = document.__v;



            if (document && document.userIdentification) {
                if (bodyData && !bodyData.userIdentification) {
                    bodyData.userIdentification = {};
                }
                if (document && document.userIdentification && document.userIdentification.salt) {
                    bodyData.userIdentification["salt"] = document.userIdentification.salt;
                }
                if (document && document.userIdentification && document.userIdentification.password) {
                    bodyData.userIdentification["password"] = document.userIdentification.password;
                }
                if (document && document.userIdentification && document.userIdentification.authRealm) {
                    bodyData.userIdentification["authRealm"] = document.userIdentification.authRealm;
                }
            }
            body.lastUpdatedBy = updatedBy;

            // var updated = _.mergeWith(document, bodyData, self._customizer);
            var updated = new self.model(bodyData);
            updated.isNew = false;

            Object.keys(bodyData).forEach(el => updated.markModified(el));
            bodyData._id = reqParams['id'];
            let updatedDoc = new self.model(bodyData);
            updatedDoc.isNew = false;
            // self.model.findOneAndUpdate({ _id: updateId }, bodyData, { new: true, overwrite: true }, function (err, response) {
            updatedDoc.save(function (err, response) {
                if (err) {
                    return self.Error(res, err, collectionName);
                }
                var logObject = {
                    'operation': 'Update',
                    'user': req.user ? req.user.username : req.headers['masterName'],
                    'originalValues': oldValues,
                    '_id': document._id,
                    'newValues': body,
                    'timestamp': new Date()
                };
                self.logger.audit(JSON.stringify(logObject));
                return self.Okay(req, res, self.getResponseObject(response), collectionName, hasNext, next);
            });
        })
    },
    _customizer: function (objValue, srcValue) {
        if (_.isArray(objValue)) {
            return srcValue;
        }
    },

    /**
     * Deletes a document from the DB. The requested document id is read from the
     * request parameters by using the {@link CrudController#idName} property.
     * @param {IncomingMessage} req - The request message object the id is read from
     * @param {ServerResponse} res - The outgoing response object
     * @returns {ServerResponse} A NO CONTENT response or NOT FOUND if no document has
     * been found for the given id
     */
    _destroy: function (req, res, hasNext, next) {
        var reqParams = params.map(req);
        var self = this;
        let collectionName = this.model.collection.collectionName;
        this.model.findOne({
            '_id': reqParams['id']
        }, function (err, document) {
            if (err) {
                return self.Error(res, err, collectionName);
            }

            if (!document) {
                return self.NotFound(reqParams['id'], res, collectionName, hasNext, next);
            }

            document.remove(function (err) {
                if (err) {
                    return self.Error(res, err, collectionName);
                }
                var logObject = {
                    'operation': 'Destory',
                    'user': req.user ? req.user.username : req.headers['masterName'],
                    '_id': document._id,
                    'timestamp': new Date()
                };
                self.logger.audit(JSON.stringify(logObject));
                return self.Okay(req, res, {}, "", hasNext, next);
            });
        });
    },

    _markAsDeleted: function (req, res, hasNext, next) {
        var reqId = Math.random().toString(36).slice(8).toUpperCase();
        var reqParams = params.map(req);
        var self = this;
        let collectionName = this.model.collection.collectionName;
        
        //Authorizing
        if (req.authorizeFlag && req.permission) {
            if (!req.permission.granted) {
                self.writeError(reqId, 'Delete', 'Not authorized: ' + reqParams['id']);
                let code = "" + (mapping[collectionName] ? mapping[collectionName] : '00') + "0" + errMsg.unAuthorized.code;
                res.status(401).send({
                    'status': statusCodes.UNAUTHORIZED.status,
                    'statusCode': code,
                    'message': "You don't have access privileges.."
                })
                return;
            }
        }

        self.writeLog(reqId, "Request", "DELETE", req.user ? req.user.username : req.headers['masterName'], reqParams['id'], new Date(), req.ip, "");
        
        this.model.findOne({
            '_id': reqParams['id'],
            deleted: false
        }, function (err, document) {
            if (err) {
                self.writeError(reqId, 'MarkAsDeleted', err);
                return self.Error(res, err, collectionName);
            }

            if (!document) {
                self.writeError(reqId, 'MarkAsDeleted', 'No Document Found with id: ' + reqParams['id']);
                return self.NotFound(reqParams['id'], res, collectionName, hasNext, next);
            }
            document.deleted = true;
            document.save(function (err) {
                if (err) {
                    self.writeError(reqId, 'MarkAsDeleted', err);
                    return self.Error(res, err, collectionName);
                }
                self.writeLog(reqId, "Response", "MarkAsDeleted", req.user ? req.user.username : req.headers['masterName'], reqParams['id'], new Date(), req.ip, {
                    message: `Document with ${document._id} deleted successfully.`,
                    _id: document._id
                });
                return self.Okay(req, res, {
                    message: `Document with _id: ${document._id} deleted successfully.`,
                    _id: document._id
                }, collectionName, hasNext, next);
            });
        });
    },

    _rucc: function (queryObject, callBack) {
        //rucc = Read Update Check Commit
        var self = this;
        return this.model.findOne({
            _id: queryObject['id'],
            deleted: false
        }).exec().then(result => {
            if (result) {
                var snapshot = result.toObject({
                    getters: false,
                    virtuals: false,
                    depopulate: true,
                });
                var newResult = callBack(result);
                if (newResult && typeof newResult.then === 'function') {
                    //newResult is a promise, resolve it and then update.
                    return newResult.then(res => {
                        self.model.findOneAndUpdate(snapshot, res, {
                            upsert: false,
                            runValidators: true
                        });
                    })
                        .exec()
                        .then(updated => {
                            if (!updated) {
                                self.__rucc(queryObject, callBack); //Re-do the transaction.
                            } else {
                                return updated;
                            }
                        });
                } else {
                    //newResult is a mongoose object
                    return self.model.findOneAndUpdate(snapshot, newResult, {
                        upsert: false,
                        runValidators: true
                    })
                        .exec()
                        .then(updated => {
                            if (!updated) {
                                self.___rucc(queryObject, callBack);
                            } else {
                                return updated;
                            }
                        });
                }
            } else {
                return null;
            }

        });
    },

    getResponseObject: function (obj) {
        return this.defaultReturn && obj[this.defaultReturn] || obj;
    }
};

CrudController.prototype = _.create(BaseController.prototype, CrudController.prototype);

/**
 * The CrudController for basic CRUD functionality on Mongoose models
 * @type {CrudController}
 */
exports = module.exports = CrudController;