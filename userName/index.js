'use strict';

const mongoose = require("mongoose");

exports.findUserName = async function(user){
    return new Promise((resolve, reject) => {
        try {
            mongoose.connection.db.collection("users", (err, userModel) => {
                userModel.findOne({_id : user, deleted : false})
                .then((resp) => {
                    if(resp && resp.userDetails && resp.userDetails.fullName){
                        return resolve(resp.userDetails.fullName);
                    } else {
                        reject({msg : "No User found"});
                    }                    
                })
                .catch((err) => {
                    reject(err);
                })
            });
            
        } catch (error) {
            reject(error);
        }
    });
}