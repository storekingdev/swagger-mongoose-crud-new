var errorMessage = {
    duplicate: {code: 0, msg: "Duplicate Record"},
    commonError: { code: 1, msg: "Something went wrong. Please try again later" },
    errorOcurred: { code: 2, msg: "Error occurred." },
    dataNotFound: { code: 3, msg: "Required Data is missing in request parameter's" },
    recordNotFound: { code: 4, msg: "Record does not exist. Please try again later." },
    unAuthorized: { code: 5, msg: "You don't have access privileges." }
}

var collectionMapping = {
    //Added collection name with respective code, 
    //Collection should be from "var crudder = new SMCrud(schema, "locationMaster", options);"
    users: 20,
    sessions: 20,
    otpValidation: 20,
    assets: 23,
    templateManagements: 24,
    notifications: 30,
    smsConfigs: 30,
    locationMaster: 27,
    rolemaster: 26,
    schedulers: 29

}

function recordNotFound(id) {
    let msg = "Record with _id: " + id + " does not exist. Please try again later."
    return msg;
}

module.exports = {
    errorMessage,
    collectionMapping,
    recordNotFound
};